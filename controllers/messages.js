const express = require('express');
const router  = express.Router();
const sms = require('../services/sms');

router.post('/', function(req, res) {
  return sms.sendMessage(req.body.to, req.body.body).then(message => {
    console.log(message);
    return res.status(200).json();
  });
});

module.exports = router;
