const express = require('express');
const router  = express.Router();

const TodoSerializer = require('../serializers/todo');
const { Todo } = require('../models');

router.get('/', function(req, res) {
  Todo.findAll().then(todos => res.json(TodoSerializer.serialize(todos)));
});

router.post('/', function(req, res) {
  Todo.create(req.body)
    .then(todo => res.status(201).json(TodoSerializer.serialize(todo)))
    .catch((error) => {
      return res.status(500).json(error);
    });
});

router.put('/:id', function(req, res) {
  return Todo
    .findById(req.params.id)
    .then(todo => {
      if (!todo) {
        return res.status(404).send({
          message: 'Todo Not Found',
        });
      }

      return todo
        .update(req.body)
        .then(updatedTodo => res.status(200).json(TodoSerializer.serialize(updatedTodo)))
        .catch(error => res.status(400).json(error));
    })
    .catch(error => res.status(400).json(error));
});

router.delete('/:id', function(req, res) {
  return Todo
    .findById(req.params.id)
    .then(todo => {
      if (!todo) {
        return res.status(404).send({
          message: 'Todo Not Found',
        });
      }

      return todo
        .destroy()
        .then(() => res.status(204).json())
        .catch(error => res.status(400).json(error));
    })
    .catch(error => res.status(400).json(error));
});

module.exports = router;
