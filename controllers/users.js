const express = require('express');
const router  = express.Router();
const jwt = require('jwt-simple');
const passport = require('passport');

const UserSerializer = require('../serializers/user');
const { User, Todo } = require('../models');

const {
  JWT_TOKEN,
  TOKEN_EXPIRATION_TIME,
} = require('../config/config');

router.get('/', function(req, res) {
  User.findAll().then((users) => {
    res.json(UserSerializer.serialize(users));
  });
});

router.get('/:id', function(req, res) {
  return User
    .findById(req.params.id, {
      include: [{
        model: Todo,
        as: 'todos',
      }],
    })
    .then((user) => {
      if (!user) {
        return res.status(404).send({
          message: 'User Not Found',
        });
      }
      return res.status(200).send(user);
    })
    .catch((error) => res.status(400).send(error));
});

router.post('/', function(req, res) {
  User.create(req.body)
    .then(user => res.status(201).send(user))
    .catch((error) => {
      if ('username' in error.fields) {
        return res.status(409).send(error);
      }
      return res.status(500).send(error);
    });
});

router.post('/login',
  passport.authenticate('local'),
  function(req, res) {
    const token = jwt.encode({
      id: req.user.id,
      expirationDate: new Date(Date.now() + TOKEN_EXPIRATION_TIME),
    }, JWT_TOKEN);

    res.status(200).send({ token });
  }
);

module.exports = router;
