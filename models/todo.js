'use strict';

module.exports = function(sequelize, DataTypes) {
  const Todo = sequelize.define('Todo', {
    content: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    complete: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  }, {
    classMethods: {
      associate: (models) => {
        Todo.belongsTo(models.User, {
          foreignKey: {
            name: 'userId',
            allowNull: false,
          },
          onDelete: 'CASCADE',
        });
      }
    }
  });

  return Todo;
};
