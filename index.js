const express = require('express')
const passport = require('passport');
const bodyParser = require('body-parser')

const usersController = require('./controllers/users');
const todosController = require('./controllers/todos');
const messagesController = require('./controllers/messages');

const app = express();

require('./initializers/passport');

app.use(passport.initialize());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/users', usersController);
app.use('/todos', todosController);
app.use('/messages', messagesController);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
});

module.exports = app;
