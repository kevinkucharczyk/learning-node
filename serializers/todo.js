'use strict';

const JSONAPISerializer = require('jsonapi-serializer').Serializer;

module.exports = new JSONAPISerializer('todos', {
  attributes: ['content', 'complete'],
});
