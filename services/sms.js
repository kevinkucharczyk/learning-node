'use strict';

const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config')[env];
const twilio = require('twilio');

let sms = {
  sendMessage(to, body) { return; },
};

if (env !== 'test') {
  const client = new twilio(config.TWILIO_SID, config.TWILIO_TOKEN);

  sms.sendMessage = function(to, body) {
    return client.messages.create({
      to,
      body,
      from: config.TWILIO_NUMBER,
    });
  };
}

module.exports = sms;
