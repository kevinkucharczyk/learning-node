'use strict';
const server = require('../../index');
const { Todo, User } = require('../../models');

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

chai.use(chaiHttp);

describe('Todos', () => {
  beforeEach(() => {
    Todo.destroy({ where: {} });
    User.destroy({ where: {} });
  });

  describe('/GET todos', () => {
    it('it should GET all todos', (done) => {
      chai.request(server)
        .get('/todos')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('data');
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST todos', () => {
    it('it should not POST todo without user field', (done) => {
      const todo = {
        content: 'test content',
        complete: false,
      };

      chai.request(server)
        .post('/todos')
        .send(todo)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.length.should.be.eql(1);
          res.body.errors[0].should.have.property('path');
          res.body.errors[0].path.should.eql('userId');
          done();
        });
    });

    it('it should not POST todo without complete field', (done) => {
      const todo = {
        content: 'test content',
        userId: 0,
      };

      chai.request(server)
        .post('/todos')
        .send(todo)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.length.should.be.eql(1);
          res.body.errors[0].should.have.property('path');
          res.body.errors[0].path.should.eql('complete');
          done();
        });
    });

    it('it should POST todo', (done) => {
      const user = User.create({
        username: 'testuser',
        password: 'testpassword',
      })
        .then(user => {
          const todo = {
            content: 'test content',
            complete: false,
            userId: user.id,
          };

          chai.request(server)
            .post('/todos')
            .send(todo)
            .end((err, res) => {
              res.should.have.status(201);
              res.body.should.be.a('object');
              res.body.should.have.property('data');
              res.body.data.should.be.a('object');
              res.body.data.should.have.property('type');
              res.body.data.should.have.property('attributes');
              res.body.data.attributes.should.have.property('content');
              res.body.data.attributes.content.should.eql(todo.content);
              res.body.data.attributes.should.have.property('complete');
              res.body.data.attributes.complete.should.eql(todo.complete);
              done();
            });
        });
    });
  });
});
