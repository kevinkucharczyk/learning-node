'use strict';
const server = require('../../index');

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const sinon = require('sinon');

const sms = require('../../services/sms');

chai.use(chaiHttp);

describe('Messages', () => {
  describe('/POST todos', () => {
    it('it should POST messages', (done) => {
      sinon.stub(sms, 'sendMessage', function() {
        return Promise.resolve();
      });

      chai.request(server)
        .post('/messages')
        .send({
          to: '+48123456789',
          body: 'test body',
        })
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });
});
